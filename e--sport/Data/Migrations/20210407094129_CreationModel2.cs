﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace e__sport.Data.Migrations
{
    public partial class CreationModel2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Prenom",
                table: "Joueur",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Nom",
                table: "Joueur",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "EquipeAppID",
                table: "Joueur",
                type: "int",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Nom",
                table: "Equipe",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Joueur_EquipeAppID",
                table: "Joueur",
                column: "EquipeAppID");

            migrationBuilder.AddForeignKey(
                name: "FK_Joueur_Equipe_EquipeAppID",
                table: "Joueur",
                column: "EquipeAppID",
                principalTable: "Equipe",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Joueur_Equipe_EquipeAppID",
                table: "Joueur");

            migrationBuilder.DropIndex(
                name: "IX_Joueur_EquipeAppID",
                table: "Joueur");

            migrationBuilder.DropColumn(
                name: "EquipeAppID",
                table: "Joueur");

            migrationBuilder.AlterColumn<string>(
                name: "Prenom",
                table: "Joueur",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<string>(
                name: "Nom",
                table: "Joueur",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<string>(
                name: "Nom",
                table: "Equipe",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");
        }
    }
}
