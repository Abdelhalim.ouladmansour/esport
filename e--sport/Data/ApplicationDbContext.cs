﻿using e__sport.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace e__sport.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<Joueur> Joueur
        {
            get; set;
        }

        public DbSet<Equipe> Equipe
        {
            get; set;
        }

        public DbSet<Competition> Competition
        {
            get; set;
        }

    }
}
