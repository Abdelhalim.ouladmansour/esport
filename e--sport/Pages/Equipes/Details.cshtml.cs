﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using e__sport.Data;
using e__sport.Models;

namespace e__sport.Pages.Equipes
{
    public class DetailsModel : PageModel
    {
        private readonly e__sport.Data.ApplicationDbContext _context;

        public DetailsModel(e__sport.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public Equipe Equipe { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            // Equipe = await _context.Equipe.FirstOrDefaultAsync(m => m.ID == id);
            Equipe = await _context.Equipe
            .Include(e => e.JoueursInscrits)
            .AsNoTracking()
            .FirstOrDefaultAsync(m => m.ID == id);

            if (Equipe == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
