﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using e__sport.Data;
using e__sport.Models;

namespace e__sport.Pages.Joueurs
{
    public class IndexModel : PageModel
    {
        private readonly e__sport.Data.ApplicationDbContext _context;

        public IndexModel(e__sport.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<Joueur> Joueur { get;set; }

        public async Task OnGetAsync()
        {
            Joueur = await _context.Joueur
                .Include(j => j.EquipeApp).ToListAsync();
        }
    }
}
