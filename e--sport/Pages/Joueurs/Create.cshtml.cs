﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using e__sport.Data;
using e__sport.Models;

namespace e__sport.Pages.Joueurs
{
    public class CreateModel : PageModel
    {
        private readonly e__sport.Data.ApplicationDbContext _context;

        public CreateModel(e__sport.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["EquipeAppID"] = new SelectList(_context.Equipe, "ID", "Nom");
            return Page();
        }

        [BindProperty]
        public Joueur Joueur { get; set; }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Joueur.Add(Joueur);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
