﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using e__sport.Data;
using e__sport.Models;

namespace e__sport.Pages.Joueurs
{
    public class DetailsModel : PageModel
    {
        private readonly e__sport.Data.ApplicationDbContext _context;

        public DetailsModel(e__sport.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public Joueur Joueur { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Joueur = await _context.Joueur
                .Include(j => j.EquipeApp).FirstOrDefaultAsync(m => m.ID == id);

            if (Joueur == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
