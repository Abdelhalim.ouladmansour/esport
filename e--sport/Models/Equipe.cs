﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace e__sport.Models
{
    public class Equipe
    {
        public int ID { get; set; }
        
        [Required]
        public string Nom { get; set; }

        public int Score { get; set; }

        // Lien de navigation 
        public ICollection<Joueur> JoueursInscrits { get; set;}


        [Display(Name = "Nombre d'inscrits")]
        public int NbInscrits
        {
            get
            {
                if (JoueursInscrits != null)

                    return JoueursInscrits.Count;
                else return -1;
            }
        }


    }
}
