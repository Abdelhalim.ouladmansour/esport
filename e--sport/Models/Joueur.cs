﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace e__sport.Models
{
    public class Joueur
    {
        public int ID { get; set; }
        
        [Required]
        public string Nom { get; set; }
        
        [Required]
        public string Prenom { get; set; }
        [Display(Name = "Date naissance")]
        [DataType(DataType.Date)]
        public DateTime Naissance { get; set; }

       public int? EquipeAppID { get; set; } 
        // Lien de navigation 
       public Equipe EquipeApp { get; set; }

    }
}
